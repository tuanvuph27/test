import React, { Component } from "react";
import Home from "./Pages/Home";
import ProductList from "./Pages/ProductList";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MainFooter from "./Component/Footer/MainFooter";
import MainHeader from "./Component/Header/MainHeader";
import ProductDetail from "./Pages/ProductDetail"
import Cart from "./Pages/Cart"
import Login from "./Pages/Login"
import Profile from "./Pages/Profile"

class App extends Component {
  render() {
    return (
      <Router>
        <MainHeader />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/list">
            <ProductList />
          </Route>
          <Route path="/detail" render={props =>  <ProductDetail {...props}/>}>
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/profile">
            <Profile />
          </Route>
        </Switch>
        <MainFooter />
      </Router>
    );
  }
}

export default App;
