import React, {Component} from "react"
import Connect from "./Connect"
import Follow from "./Follow"
import "./MainContact.css"

class MainContact extends Component{
    render() {
        return (
            <div className="col-lg-3 col-md-6 col-12">
                {/* Single Widget */}
                <div className="single-footer social">
                    <h4 className="MainContact">Liên Lạc</h4>
                    {/* Single Widget */}
                    <Connect />
                    {/* End Single Widget */}
                    <Follow />
                </div>
                {/* End Single Widget */}
            </div>
        )
    }
}

export default MainContact