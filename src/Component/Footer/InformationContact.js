import React, {Component} from "react"
import "./InformationContact.css"

class InformationContact extends Component{
    render(){
        return(
            <div className="col-lg-5 col-md-6 col-12 information-contact ">
                <div className="about">
                    <div className="logo">
                        <a href="index.html">Pshop<span>.</span></a>
                    </div>
                    <p className="text">Praesent dapibus, neque id cursus ucibus, tortor neque egestas augue,  magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
                    <p className="call">Bất Kỳ Câu Hỏi? Liên Hệ Chúng Tôi 24/7<br /><span>+0123 456 789</span></p>
                </div>				
            </div>
        )
    }
}

export default InformationContact