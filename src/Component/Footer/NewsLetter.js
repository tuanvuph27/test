import React, {Component} from "react"
import "./NewsLetter.css"

class NewsLetter extends Component{
    render(){
        return(
            <section className="newsletter section">
                <div className="container">
                    <div className="inner-top">
                        <div className="row">
                            <div className="col-lg-8 offset-lg-2 col-12">
                            {/* Start Newsletter Inner */}
                                <div className="inner">
                                    <h4>Thông Tin</h4>
                                    <p> Theo dõi thông tin và giảm <span>10%</span> cho lần mua sắm đầu tiên</p>
                                    <form action="mail/mail.php" method="get" target="_blank" className="newsletter-inner">
                                    <input name="EMAIL" placeholder="Email của bạn..." required type="email" />
                                    <button className="btn">Theo Dõi</button>
                                    </form>
                                </div>
                            {/* End Newsletter Inner */}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default NewsLetter