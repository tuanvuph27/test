import React, {Component} from "react"
import InfCopyright from "./InfCopyright"
import Payment from "./Payment"
import "./MainCopyright.css"

class MainCopyright extends Component{
    render() {
        return (
            <div className="copyright col-lg-12">
                <InfCopyright />
                <Payment />
            </div>
        );
    }
}

export default MainCopyright