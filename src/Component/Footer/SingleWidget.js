import React, {Component} from "react"
import "./SingleWidget.css"

const singleWidget = [
    {
        singleTitle : "Thông Tin",
        infLi: ["Về Chung Tôi", "Việt Nam", "Điều Khoản", "Liên Hệ","Giúp Đỡ"]
    },
    {
        singleTitle : "Dịch Vụ",
        infLi: ["Thanh Toán", "Hoàn Tiền", "Hoàn Trả", "Chuyển Hàng","Bảo Mật"]
    }    
]
class SingleWidget extends Component{
    render(){
        return(
            <div className="row col-lg-4 col-md-6 col-12 SingleWidget">
                {singleWidget.map((item, index)=>(
                <div className="col-lg-6 col-md-12 col-12" key={index}>                    
                    <div className="single-footer links">
                        <h4>{item.singleTitle}</h4>
                        {item.infLi.map((child, number)=>(
                            <ul key={number}>
                                <li><a>{child}</a></li>
                            </ul>
                        ))}                        
                    </div>                               
                </div>
                ))} 
            </div> 
        )
    }
}

export default SingleWidget