import React,{Component} from "react"
import {getData} from "../../../redux/actions"
import {connect} from "react-redux"
import "./SingleProductInfo.css"
import {Link} from "react-router-dom"

class SingleProductInfo extends Component{
    constructor(props){
        super(props)
        this.state ={
            page : 1 ,
            query : "ao khoac"       
        }
    }
    componentDidMount(){
        this.props.getData(this.state.page, this.state.query)       
    }
    render(){
        const a = this.props.productList.slice(0, 8)
        return(           
            <div className="row single-product-info-container"> 
                {a.length!=0 && a.map((item, index)=>(
                    <div className="single-product col-xl-3 col-lg-4 col-md-4 col-12" key={index}>
                        <Link to={`/detail?id=${item.product_id}`} style={{textDecoration : "none"}}>
                            <div className="product-img">
                                <img src={item.img_url} alt="#" />
                            </div>
                            <div className="product-content">
                                <h3>{item.name}</h3>
                                <div className="product-price">
                                    <span>{item.original_price}</span>
                                </div>
                            </div>
                        </Link>
                    </div>
                ))}               
            </div>
        )
    }
}

const mapStateToProps = state =>({
    productList : state.productList
})

const mapDispatchToProps = dispatch =>({
    getData: (page, query)=> dispatch(getData(page, query))
})

export default connect(mapStateToProps, mapDispatchToProps)(SingleProductInfo)