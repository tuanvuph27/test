import React, {Component} from "react"
import "./NavMain.css"
import {connect} from "react-redux"
import {getData} from "../../../redux/actions"
import {Link} from "react-router-dom"

class NavMain extends Component{
    render(){
        return(
            <div className="nav-main-option nav-main">
                {/* Tab Nav */}
                <ul className="nav-main__tabs nav" id="myTab" role="tablist">
                    <li className="nav-item" onClick={()=>this.props.getData(1,"ao khoac moi")}><Link className="nav-link active" data-toggle="tab" role="tab">Áo Khoác</Link></li>
                    <li className="nav-item" onClick={()=>this.props.getData(1, "ao thun moi")}><Link className="nav-link" data-toggle="tab" role="tab">Áo Thun</Link></li>
                    <li className="nav-item" onClick={()=>this.props.getData(1, "so mi moi")}><Link className="nav-link" data-toggle="tab" role="tab">Sơ Mi</Link></li>
                    <li className="nav-item" onClick={()=>this.props.getData(1, "quan jean moi")}><Link className="nav-link" data-toggle="tab" role="tab">Quần Jean</Link></li>
                    <li className="nav-item" onClick={()=>this.props.getData(1, "quan kaki moi")}><Link className="nav-link" data-toggle="tab" role="tab">Quần Kaki</Link></li>
                    <li className="nav-item" onClick={()=>this.props.getData(1, "mu moi")}><Link className="nav-link" data-toggle="tab" role="tab">Mũ</Link></li>
                </ul>
                {/*/ End Tab Nav */}
            </div>
        )
    }
}

const mapDispatchToProps = dispatch =>({
    getData: (page, query)=> dispatch(getData(page, query))
})

export default connect(null, mapDispatchToProps)(NavMain)