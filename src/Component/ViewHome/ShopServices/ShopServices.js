import React, {Component} from "react"
import "./ShopServices.css"

const services = [
    {
        class : "fas fa-rocket",
        title: "Miễn Phí Ship",
        describe: "Hóa Đơn Từ 199k"
    },
    {
        class : "fas fa-retweet",
        title: "Hoàn Trả",
        describe: "Trong 30 Ngày"
    },
    {
        class : "fas fa-lock",
        title: "Thanh Toán",
        describe: "Bảo Mật 100%"
    },
    {
        class : "fas fa-tags",
        title: "Giá Tốt",
        describe: "Giá Tốt Nhất"
    }
]
class ShopServices extends Component{
    render(){
        return(
            <div className="shop-services section home">
                <div className="container">
                    <div className="row">
                        {services.map((item, index)=>(
                            <div className="col-lg-3 col-md-6 col-12" key={index}>
                            {/* Start Single Service */}
                                <div className="single-service">
                                    <i className={item.class} />
                                    <h4>{item.title}</h4>
                                    <p>{item.describe}</p>
                                </div>
                            {/* End Single Service */}
                            </div>
                        ))}                      
                    </div>
                </div>
            </div>
        )
    }
}

export default ShopServices