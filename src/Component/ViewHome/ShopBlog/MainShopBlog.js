import React,{Component} from "react"
import SingleBlog from "./SingleBlog"
import "./MainShopBlog.css"

class MainShopBlog extends Component{
    render(){
        return(
            <div className="main-shop-blog section">
                <div>
                    <div className="row">
                        <div className="col-12">
                            <div className="section-title">
                                <h2>NHẬT KÝ MUA HÀNG</h2>
                            </div>
                        </div>
                    </div>
                    <div>
                        <SingleBlog />
                    </div>                  
                </div>
            </div>
        )
    }
}

export default MainShopBlog