import React,{Component} from "react"
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import "./MainSliderArea.css"

class MainSliderArea extends Component{
    constructor(props){
        super(props)
        this.state = {
            imgbanner:[
                {id:1, imgsrc: "http://media3.scdn.vn/img4/2020/05_20/1GYAAJMcnyNCuOAMWkoK.png"},
                {id:1, imgsrc: "http://media3.scdn.vn/img4/2020/05_20/a0CYZWSr1IsFuOykXlnH.png"},
                {id:1, imgsrc: "http://media3.scdn.vn/img4/2020/05_20/a4W4OH4UAHGHswqvFSEm.png"},
            ]
        }
    }
    render(){
        const { imgbanner } = this.state;
        const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1, 
            autoplay: true,
            speed: 5000,
        };
        return(
            <div className="main-slider-area">
                <Slider {...settings}>
                    {imgbanner.map(itemimg => <div key={itemimg.id} className="main-slider-area-img"><img src={itemimg.imgsrc} /></div>)}
                </Slider>
            </div>
        )
    }
}

export default MainSliderArea