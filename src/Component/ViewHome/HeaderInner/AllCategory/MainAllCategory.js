import React, {Component} from "react"
import "./MainAllCategory.css"
import NewArrivals from "./NewArrivals"
import BestSelling from "./BestSelling"
import {Link} from "react-router-dom"
import {connect} from "react-redux"
import {search} from "./../../../../redux/actions"

class MainAllCategory extends Component{
    render(){
        return(
            <div className="col-lg-3">
                <div className="main-all-category">
                    <h3 className="cat-heading"><i className="fa fa-bars" aria-hidden="true" />DANH MỤC</h3>
                    <ul className="main-all-category__child">
                        <NewArrivals />
                        <BestSelling />
                        <li onClick={()=>this.props.search("phu kien thoi trang")}><Link to="/list">phụ kiện</Link></li>
                        <li onClick={()=>this.props.search("kinh mat")}><Link to="/list">Kính Mát</Link></li>
                        <li onClick={()=>this.props.search("dong ho")}><Link to="/list">Đồng Hồ</Link></li>
                        <li onClick={()=>this.props.search("thoi trang nam")}><Link to="/list">thời trang nam</Link></li>
                        <li onClick={()=>this.props.search("thoi trang nu")}><Link to="/list">thời trang nữ</Link></li>
                    </ul>
                </div>
            </div>               
        )
    }
}

const mapDispatchToProps = dispatch =>({
    search : (query)=> dispatch(search(query))
})

export default connect(null, mapDispatchToProps)(MainAllCategory)