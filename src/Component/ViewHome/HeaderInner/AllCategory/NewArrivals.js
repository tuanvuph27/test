import React, {Component} from "react"
import "./NewArrivals.css"
import {Link} from "react-router-dom"
import {connect} from "react-redux"
import {search} from "./../../../../redux/actions"

class NewArrivals extends Component{
    render() {
        return (
            <li><a>Sản Phẩm Mới <i className="fa fa-angle-right" aria-hidden="true" /></a>
                <ul className="newArrivals-sub-category">
                    <li onClick={()=>this.props.search("ao khoac")}><Link to="/list">áo khoác</Link></li>
                    <li onClick={()=>this.props.search("ao thun")}><Link to="/list">áo thun</Link></li>
                    <li onClick={()=>this.props.search("ao so mi")}><Link to="/list">sơ mi</Link></li>
                    <li onClick={()=>this.props.search("quan jean")}><Link to="/list">quần jean</Link></li>
                    <li onClick={()=>this.props.search("quan kaki")}><Link to="/list">quần kaki</Link></li>
                    <li onClick={()=>this.props.search("mu")}><Link to="/list">mũ</Link></li>
                </ul>
            </li>
        );
    }
}

const mapDispatchToProps = dispatch=>({
    search: (query)=>dispatch(search(query))
})

export default connect(null, mapDispatchToProps)(NewArrivals)