import React, {Component} from "react"
import "./MenuArea.css"
import {Link} from "react-router-dom"

class MenuArea extends Component{
    render(){
        return(
            <div className="col-lg-9 col-12">
                <div className="menu-area">
                    <div className="navbar navbar-expand-lg menu-area__fix">
                        <div className="navbar-collapse">	
                            <div className="nav-inner">	
                                <ul className="nav main-menu menu navbar-nav menu-area">
                                    <li className="active"><Link to="/">Trang Chủ</Link></li>
                                    <li><a>Sản Phẩm</a></li>												
                                    <li><a>Dịch Vụ</a></li>
                                    <li><a>Cửa Hàng   <i className="fas fa-angle-down" /><span className="new">Mới</span></a>
                                        <ul className="dropdown">
                                            <li><Link to="/list">Danh sách sản phẩm</Link></li>
                                            <li><Link to="/cart">Giỏ hàng</Link></li>
                                            <li><Link to="/">Thoát</Link></li>
                                        </ul>
                                    </li>
                                    <li><a>Thông Tin</a></li>									
                                    <li><a>Liên Hệ</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default MenuArea