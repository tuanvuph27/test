import React, {Component} from "react"
import "./FilterPrice.css"
import {connect} from "react-redux"
import {filterPrice, getData} from "./../../../redux/actions"

class FilterPrice extends Component{
    constructor(props){
        super(props)
        this.state={
            min: "",
            max : ""
        }
    }
    handleSubmit= event => {
        if(this.state.min !== "" && this.state.max !== ""){
            this.props.filterPrice(this.state.min, this.state.max) 
            event.preventDefault(); 
        }else{
            alert ("Bạn chưa nhập đủ thông tin")
            event.preventDefault(); 
        }
    }
    render() {
        return (            
            <div className="filter-price">
                <h3 className="title">THEO KHOẢNG GIÁ</h3>
                <form className="filter-price__form" onSubmit={this.handleSubmit}>
                    <input value={this.state.min} onChange={(e)=> this.setState({min : e.target.value})} placeholder="Thấp nhất" type="number" className="input-price"/>
                    <span className="to-label"><span className="arrow-right"></span></span>
                    <input value={this.state.max} onChange={(e)=> this.setState({max : e.target.value})} placeholder="Cao nhất" type="number" className="input-price"/>
                    <button type="submit" className="submit-button"></button>
                </form>
                <div className="filter-price__btn">
                    <button onClick={()=>this.props.filterPrice(0, 90000)} className={this.props.dataPrice[0] === 0 && this.props.dataPrice[1] === 90000 ? "active-price" : "" } ><span>DƯỚI 90K</span></button>
                    <button onClick={()=>this.props.filterPrice(90000, 130000)} className={this.props.dataPrice[0] === 90000 && this.props.dataPrice[1] === 130000 ? "active-price" : "" }><span>90K ~ 130K</span></button>
                    <button onClick={()=>this.props.filterPrice(130000, 200000)} className={this.props.dataPrice[0] === 130000 && this.props.dataPrice[1] === 200000 ? "active-price" : "" }><span>130K ~ 200K</span></button>
                    <button onClick={()=>this.props.filterPrice(200000, 300000)} className={this.props.dataPrice[0] === 200000 && this.props.dataPrice[1] === 300000 ? "active-price" : "" }><span>200K ~ 300K</span></button>
                    <button onClick={()=>this.props.filterPrice(300000, 600000)} className={this.props.dataPrice[0] === 300000 && this.props.dataPrice[1] === 600000 ? "active-price" : "" }><span>300K ~ 600K</span></button>
                    <button onClick={()=>this.props.filterPrice(600000, -1)} className={this.props.dataPrice[0] === 600000 && this.props.dataPrice[1] === -1 ? "active-price" : ""}><span>TRÊN 600K</span></button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state =>({
    productList : state.productList,
    dataPrice : state.dataPrice
})

const mapDispatchToProps = dispatch =>({
    filterPrice : (min, max)=> dispatch(filterPrice(min, max)),
    getData : ()=>dispatch(getData())
})

export default connect(mapStateToProps, mapDispatchToProps)(FilterPrice)