import React, {Component} from "react"
import "./Breadcrumbs.css"

class Breadcrumbs extends Component{
    render() {
        return (
            <div className="col-12 breadcrumbs">
                <div className="bread-inner">
                    <ul className="bread-list">
                        <li>Trang Chủ<i className="fas fa-arrow-right" /></li>
                        <li className="active">Danh Sách Sản Phẩm</li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default Breadcrumbs