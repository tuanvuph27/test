import React, {Component} from "react"
import {connect} from "react-redux"
import {getData, filterCheckbox} from "./../../../redux/actions"
import "./SingleProduct.css"
import truncate from 'lodash/truncate';
import sanitizeHtml from 'sanitize-html';
import {Link} from "react-router-dom"

class SingleProduct extends Component{
    componentDidMount(){
        this.props.getData(1, this.props.query);
    }
    render() {
        let {productListFinal} = this.props;
        console.log(productListFinal)
        return (
            <div className="row">
                {productListFinal.length >0 && productListFinal.map((item, index)=>(                 
                    <div className="single-product col-lg-4 col-md-6 col-12" key={index}>
                        <div className="single-product-content">
                            <Link to = {`/detail?id=${item.product_id}`} style={{ textDecoration: 'none' }}>
                                <div className="single-product__img">
                                    <img className="default-img" src={item.img_url} alt="#" />                                    
                                </div>
                                <div className="product-content">
                                <h3>{truncate(sanitizeHtml(item?.name),{length: 25,separator: /,? +/,})}</h3>
                                    <div className="product-price">
                                    <span>{item.min_price}</span>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                ))}              
            </div>
        );
    }
}

const mapStateToProps = state =>({
    query : state.query,
    productListFinal : state.productListFinal,
    productList : state.productList,
    filterPrice : state.filterPrice,
})

const mapDispatchToProps = dispatch =>({
    getData : (page, query)=> dispatch(getData(page, query)),
    filterCheckbox : ()=>dispatch(filterCheckbox())
})

export default connect(mapStateToProps, mapDispatchToProps)(SingleProduct)