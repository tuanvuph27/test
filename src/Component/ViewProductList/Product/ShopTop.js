import React,{Component} from "react"
import "./ShopTop.css"
import {getData ,setOrderNameAZ, setOrderNameZA, setOrderPriceAZ, setOrderPriceZA} from "./../../../redux/actions"
import {connect} from "react-redux"

class ShopTop extends Component{
    constructor(props) {
        super(props)
        this.state = {
            page: 1,
            option: "Tùy chọn"
        }
    }
    handlePage = (page) => {
        this.setState({
            page
        })
        this.props.getData(page, this.props.userData)
    }
    handleChangeOption1=(value)=>{
        this.setState({
            option : value
        })
        this.props.setOrderNameAZ()
    }
    handleChangeOption2=(value)=>{
        this.setState({
            option : value
        })
        this.props.setOrderNameZA()
    }
    handleChangeOption3=(value)=>{
        this.setState({
            option : value
        })
        this.props.setOrderPriceAZ()
    }
    handleChangeOption4=(value)=>{
        this.setState({
            option : value
        })
        this.props.setOrderPriceZA()
    }
    render(){
        let {page} = this.state;
        let disabled = page == 1? true : false
        return(
            <div className="shop-top">
                <div className="shop-shorter">    
                    <div className="single-shorter">
                        <label>Sắp xếp :</label>
                        <div className="dropdown">
                            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {this.state.option}
                            </button>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a className="dropdown-item" onClick={()=>this.handleChangeOption1("Tên: A->Z")}><span>Tên: A->Z</span></a>
                                <a className="dropdown-item" onClick={()=>this.handleChangeOption2("Tên: Z->A")}>Tên: Z->A</a>
                                <a className="dropdown-item" onClick={()=>this.handleChangeOption3("Giá: Thấp->Cao")}>Giá: Thấp->Cao</a>
                                <a className="dropdown-item" onClick={()=>this.handleChangeOption4("Giá: Cao->Thấp")}>Giá: Cao->Thấp</a>

                            </div>
                        </div>
                    </div>
                    <div className="shop-top-arrow">
                        <button onClick={()=>this.handlePage(page -1)} disabled={disabled} className={page == 1 && "hide"}><i className="fas fa-chevron-left"></i></button>
                        <input value={this.state.page}/>
                        <button onClick={() =>this.handlePage(page + 1)}><i className="fas fa-chevron-right"></i></button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state =>({
    productList : state.productList,
    productListFinal : state.productListFinal,
    userData : state.query,
})

const mapDispatchToProps = dispatch =>({
    getData : (page, query)=>dispatch(getData(page, query)),
    setOrderNameAZ: ()=>dispatch(setOrderNameAZ()),
    setOrderNameZA : ()=>dispatch(setOrderNameZA()),
    setOrderPriceAZ: ()=>dispatch(setOrderPriceAZ()),
    setOrderPriceZA: ()=>dispatch(setOrderPriceZA())
})
export default connect(mapStateToProps, mapDispatchToProps)(ShopTop)