import React, { Component } from 'react';
import {connect} from "react-redux"
import "./SizeColorProductDetail.css"
import {chooseSize} from "./../../redux/actions"

class SizeProductDetail extends Component {
    render() {      
        let {product} = this.props
        let attribute = product.attribute && product.attribute.length>0 && product.attribute[0]
        let missing = null
        if(this.props.size === "" && this.props.warning === true){
            missing = <img style={{marginLeft : '2%'}} src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCI+CiAgICA8ZGVmcz4KICAgICAgICA8cGF0aCBpZD0iYSIgZD0iTTEyIDJDNi40OCAyIDIgNi40OCAyIDEyczQuNDggMTAgMTAgMTAgMTAtNC40OCAxMC0xMFMxNy41MiAyIDEyIDJ6bTEgMTVoLTJ2LTJoMnYyem0wLTRoLTJWN2gydjZ6Ii8+CiAgICA8L2RlZnM+CiAgICA8dXNlIGZpbGw9IiNEMDAyMUIiIGZpbGwtcnVsZT0ibm9uemVybyIgeGxpbms6aHJlZj0iI2EiLz4KPC9zdmc+Cg==" />
        }
        return (
            <div className="size-product-detail">
                {attribute && <span className="size-product-detail__name col-lg-2">{attribute.name}:</span>}
                <div className="size-product-detail__size col-lg-10">
                    {attribute && attribute.value.length >0 && attribute.value.map((size,index)=>
                        <div className="button-size" key={index}>
                            <button onClick={()=>this.props.chooseSize(size.value)} className={this.props.size === size.value ? "active-size" : ""} type="button">{size.value}</button>
                        </div>                     
                    )}
                    {missing}  
                </div>              
            </div>
        );
    }
}

const mapStateToProps= state=>({
    product : state.product,
    size : state.size,
    color : state.color,
    warning : state.warning
})

const mapDispatchToProps = dispatch =>({
    chooseSize : (sizeProduct)=>dispatch(chooseSize(sizeProduct))
})

export default connect(mapStateToProps, mapDispatchToProps)(SizeProductDetail);