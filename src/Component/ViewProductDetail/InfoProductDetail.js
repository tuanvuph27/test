import React, { Component } from "react";
import { connect } from "react-redux";
import "./InfoProductDetail.css";
import SizeProductDetail from "./SizeProductDetail"
import ColorProductDetail from "./ColorProductDetail"
import QuantityProductDetail from "./QuantityProductDetail"
import {addToCart, missingAttribute, fixWarning} from "./../../redux/actions"
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import {Link} from "react-router-dom"

class InfoProductDetail extends Component {
  handleAddToCart=(product)=>{
    let {size, color} = this.props
    if(size === "" || color === ""){
      this.props.missingAttribute()
    }else{
      const item = {
        id: product && product.id,
        name: product && product.name, 
        price: product && product.final_price , 
        pic:`http://media3.scdn.vn${this.props.images}`,
        quantity: this.props.count,
        size: this.props.size,
        color: this.props.color
      };
      this.props.addToCart(item)
      this.props.fixWarning()
    }
  }
  render() {
    let { product } = this.props;
    let discount = null;
    if (product.promotion_percent > 0) {
      discount = <span className="discount">Giảm {product.promotion_percent}%</span>
    }

    let finalPrice = null
    if(product.promotion_percent > 0){
      finalPrice = <NumberFormat value={product.price} displayType={'text'} thousandSeparator={true} renderText={value => <span className="final-price">{value.replace(',','.')}đ</span>} />
    } 
    let missing = null
      if(this.props.warning === true){
        missing = <div style={{color: '#e5101d'}}>* Vui lòng chọn đầy đủ thuộc tính</div>
    }
    let attribute = product.attribute
    console.log(product && attribute)
    return (
        <div className="info-product-detail col-lg-8">
            <h3>
            {product.name} -{" "}
            {product.categories && product.categories.category_level2_name} -{" "}
            {product.categories && product.categories.category_level3_name}
            </h3>
            <div className="price-code"> 
              <div className="info-product-detail__price">
                  {discount}
                  <NumberFormat value={product.final_price} displayType={'text'} thousandSeparator={true} renderText={value => <span className={`price ${product.promotion_percent === 0 && "price-fix"}`}>{value.replace(',','.')}đ</span>} />                  
                  {finalPrice}
              </div>
            </div>
            <div class="support-shipping">
                <h5>Hỗ Trợ Vận Chuyển:</h5>
                <span>
                    {product.shipping_support && product.shipping_support.length > 0 && product.shipping_support.map((item,index)=>
                        <div style={{color:'#333'}} key={index}>
                            - Hỗ trợ vận chuyển tới 
                            <NumberFormat value={item.seller_support_fee} displayType={'text'} thousandSeparator={true} renderText={value => <span style={{fontWeight: 500,padding : '0 0.6%'}}>{value.replace(',','.')}đ</span>} />
                             cho đơn hàng từ 
                            <NumberFormat value={item.order_amount} displayType={'text'} thousandSeparator={true} renderText={value => <span style={{fontWeight: 500,padding : '0 0.6%'}}>{value.replace(',','.')}đ</span>} />                             
                        </div>
                    )}
                </span>
            </div>
            <div className="product-option">
              <SizeProductDetail />
              <ColorProductDetail />
              <QuantityProductDetail />
              {missing}
              <button type="button" onClick={()=>this.handleAddToCart(product)} className="add-to-cart">Thêm vào giỏ hàng</button>
              <Link to="/cart"><button type="button" className="to-cart">Xem giỏ hàng</button></Link>
            </div>
        </div>
    );
  }
}

InfoProductDetail.propTypes = {
  product: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  product: state.product,
  cartList : state.cartList,
  color: state.color,
  size : state.size,
  warning: state.warning,
  images : state.images,
  count : state.count
});

const mapDispatchToProps = dispatch =>({
  addToCart:(productCart)=>dispatch(addToCart(productCart)),
  missingAttribute: ()=>dispatch(missingAttribute()),
  fixWarning: ()=>dispatch(fixWarning())
})

export default connect(mapStateToProps, mapDispatchToProps)(InfoProductDetail);
