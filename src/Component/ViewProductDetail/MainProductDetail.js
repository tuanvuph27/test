import React, { Component } from "react";
import { connect } from "react-redux";
import { dataProductDetail,changeImgDetail } from "./../../redux/actions";
import * as queryString from "query-string";
import "./MainProductDetail.css";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

class MainProductDetail extends Component {
    componentDidMount() {
        const params = queryString.parse(window.location.search);
        this.props.dataProductDetail(params.id);
    }

    render() {
      const settings = {
          infinite: true,
          speed: 500,
          slidesToShow: 4,
          slidesToScroll: 1
      };
      let {product } = this.props
      let images = this.props.images 
      let productOption = null
      if(product.images && product.images.length < 5){
          productOption = product.images && product.images.length < 5 && product.images.map((imgproduct,index)=>
                              <div className="option-product-div main-option-product-fix col-lg-3" key={index}>
                                  <img onClick={()=>this.props.changeImgDetail(imgproduct)} className={`option-product ${images === imgproduct && "active"}`} src={`http://media3.scdn.vn${imgproduct}`} />
                              </div>
                          )                                                   
      }else if(product.images && product.images.length > 4){
          productOption = <Slider {...settings}>
                              {product.images && product.images.length > 4 && product.images.map((imgproduct,index)=>
                                  <div className="option-product-div main-option-product" key={index}>
                                      <img onClick={()=>this.props.changeImgDetail(imgproduct)} className={`option-product ${images === imgproduct && "active"}`} src={`http://media3.scdn.vn${imgproduct}`} />
                                  </div>
                              )}
                          </Slider>
      }
    return (
      <div className="main-product-detail col-lg-4">        
        {product.images && product.images.length >0 && 
          <div className="main-product-detail__img">
            <img src={`http://media3.scdn.vn${images}`}/>
          </div>
        }
        <div className={product.images && product.images.length > 4? "main-product-detail__option" : "main-product-detail__option2"}>
          {productOption}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product,
  images : state.images
});

const mapDispatchToProps = dispatch => ({
  dataProductDetail: (id) => dispatch(dataProductDetail(id)),
  changeImgDetail:(img)=>dispatch(changeImgDetail(img))
});

export default connect(mapStateToProps, mapDispatchToProps)(MainProductDetail);
