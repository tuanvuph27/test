import React, { Component } from 'react';
import LogoShopDetail from "./LogoShopDetail"
import "./MainInfoShop.css"
import ProductTab from "./ProductTab"

class MainInfoShop extends Component {
    render() {
        return (
            <div className="main-info-shop">
                <LogoShopDetail />
                <ProductTab />
            </div>
        );
    }
}

export default MainInfoShop;