import React, { Component } from 'react';
import "./LogoShopDetail.css"
import {connect} from "react-redux"

class LogoShopDetail extends Component {
    render() {
        let info = this.props.product && this.props.product.shop_info
        let percent = info && info.good_review_percent
        let percentfinal = percent && percent.toFixed(0)
        return (
            <div className="logo-shop-detail">
                <div className="logo-shop-detail__img">
                    <img src={info && info.shop_logo}/>
                </div>
                <div className="logo-shop-detail__info">
                    <div className="name">{info && info.shop_name}</div>
                    <div className="contact"><span style={{fontWeight : '500', color : 'rgb(75, 196, 236)'}}>Liên hệ: </span><span style={{color: '#333', fontWeight:450}}>{info && info.phone_number}</span></div>
                    <div>Đánh giá tốt: <span style={{color :'red', fontWeight: '500'}}>{percentfinal && percentfinal}%</span></div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>({
    product : state.product
})

export default connect(mapStateToProps, null)(LogoShopDetail);