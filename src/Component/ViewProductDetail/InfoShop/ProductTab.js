import React, { Component } from 'react';
import "./ProductTab.css"
import {connect} from "react-redux"

class ProductTab extends Component {
    render() {
        let {product} = this.props
        return (
            <div className="product-tab">
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                <li className="nav-item" role="presentation">
                    <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">CHI TIẾT SẢN PHẨM</a>
                </li>
                <li className="nav-item" role="presentation">
                    <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">ĐÁNH GIÁ</a>
                </li>
                <li className="nav-item" role="presentation">
                    <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">HỎI ĐÁP</a>
                </li>
                </ul>
                <div className="tab-content" id="myTabContent">
                <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div className="product-description">
                        {product && product.short_description}
                    </div>
                    <div className="product-img">
                        {product.images && product.images.length >0 && product.images.map((img,index)=>
                            <div key={index} className="product-img__src">
                                <img src={`http://media3.scdn.vn${img}`}/>
                            </div>
                        )}
                    </div>
                </div>
                <div className="tab-pane fade review" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div className="icon">
                        <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI5NiIgaGVpZ2h0PSI5NiIgdmlld0JveD0iMCAwIDk2IDk2Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjAlIiB5Mj0iMTAwJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNFRUUiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRDhEOEQ4Ii8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZmlsbD0iI0RFREVERSIgZD0iTTI5LjgxMSAzNi44M2wtMy40NjItOC4wMDhhLjU5NS41OTUgMCAwIDAtMS4wOTQgMGwtNC4xNDIgOS42MS4zNzQtLjAzNiA3LjgzMy0uNzM3YS41OTYuNTk2IDAgMCAwIC40OTEtLjgzTTkwLjQ2IDQzLjE0MmwtOS42MjYtLjg5N2EuNTk3LjU5NyAwIDAgMC0uNDUuMTQ1bC0zLjAxNCAyLjY1Ny0xMi4zMjMgMTAuODUyYS41OTYuNTk2IDAgMCAwLS4xODcuNTc3bDEuMzA1IDUuODAzYy4wOS4zOTUuNTM4LjU4OC44ODYuMzgybDIuODQ0LTEuNjg4YS41OTkuNTk5IDAgMCAxIC42MDggMGwxMS44ODcgNy4wMzljLjQ1LjI2NiAxLS4xMzMuODg1LS42NDNsLTMuMDMzLTEzLjQ3OGEuNTk4LjU5OCAwIDAgMSAuMTg4LS41NzlsMTAuMzY3LTkuMTNhLjU5NS41OTUgMCAwIDAtLjMzOC0xLjA0TTE1Ljc2OCA0Mi4xOThsLTEwLjIyNy45NmEuNTk2LjU5NiAwIDAgMC0uMzM4IDEuMDQybDEwLjM3NiA5LjEyYS42LjYgMCAwIDEgLjE4OC41NzhsLTMuMDIgMTMuNDgyYy0uMTE0LjUxLjQzNi45MS44ODQuNjQzbDExLjg4Mi03LjA1Yy4xODctLjExMS40Mi0uMTExLjYwOCAwbDMuNDU3IDIuMDQ4YS41OTYuNTk2IDAgMCAwIC44ODUtLjM4M2wxLjM3OC02LjE0OGEuNTk0LjU5NCAwIDAgMC0uMTg4LS41NzdMMTkuMzIgNDUuMDdsLTMuMTA0LTIuNzI4YS41ODkuNTg5IDAgMCAwLS40NDktLjE0NU02OS42MzggMjguODIybC0zLjQyIDcuOTNjLS4xNTkuMzcyLjA5Ljc5Mi40OTMuODI5bDcuMTMyLjY2NGEuNTk2LjU5NiAwIDAgMCAuNjAzLS44MjlsLTMuNzE1LTguNTk0YS41OTUuNTk1IDAgMCAwLTEuMDkzIDAiLz4KICAgICAgICA8cGF0aCBmaWxsPSJ1cmwoI2EpIiBkPSJNNjMuMzA4IDcyLjU0OWwtMTQuNjctOC42ODZhLjY4Mi42ODIgMCAwIDAtLjY5OCAwbC0xNC42NjIgOC42OTlhLjY4NC42ODQgMCAwIDEtMS4wMTYtLjczOEwzNS45OSA1NS4xOWEuNjg1LjY4NSAwIDAgMC0uMjE2LS42NjVMMjIuOTcxIDQzLjI3YS42ODMuNjgzIDAgMCAxIC4zODctMS4xOTRsMTYuOTczLTEuNTk2YS42ODIuNjgyIDAgMCAwIC41NjQtLjQxMWw2Ljc0OC0xNS42NTRhLjY4NC42ODQgMCAwIDEgMS4yNTctLjAwMWw2Ljc2MyAxNS42NDhjLjA5OS4yMy4zMTYuMzg3LjU2NC40MTFsMTYuOTc0IDEuNThhLjY4NC42ODQgMCAwIDEgLjM4OCAxLjE5NUw2MC43OTUgNTQuNTE1YS42ODIuNjgyIDAgMCAwLS4yMTUuNjYzbDMuNzQzIDE2LjYzMmEuNjgzLjY4MyAwIDAgMS0xLjAxNS43MzkiLz4KICAgIDwvZz4KPC9zdmc+Cg==" alt="Sản phẩm chưa có đánh giá" />
                        <div className="text">Sản phẩm chưa có đánh giá.</div>
                        <div className="title">Chọn mua sản phẩm để là người đầu tiên đánh giá sản phẩm này.</div>
                    </div>
                </div>
                <div className="tab-pane fade review" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div className="icon">
                        <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzU2IiBoZWlnaHQ9IjM1NiIgdmlld0JveD0iMCAwIDM1NiAzNTYiPgogICAgPGRlZnM+CiAgICAgICAgPHBhdGggaWQ9ImEiIGQ9Ik0wIDBoMzU2djM1NkgweiIvPgogICAgPC9kZWZzPgogICAgPGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgICAgICA8bWFzayBpZD0iYiIgZmlsbD0iI2ZmZiI+CiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI2EiLz4KICAgICAgICA8L21hc2s+CiAgICAgICAgPHVzZSBmaWxsPSIjRkZGIiBmaWxsLW9wYWNpdHk9IjAiIHhsaW5rOmhyZWY9IiNhIi8+CiAgICAgICAgPHJlY3Qgd2lkdGg9IjI0NiIgaGVpZ2h0PSIzOCIgeD0iNzUiIHk9IjcxIiBmaWxsPSIjRUVGOEZGIiBmaWxsLXJ1bGU9Im5vbnplcm8iIG1hc2s9InVybCgjYikiIHJ4PSIxOSIgdHJhbnNmb3JtPSJyb3RhdGUoLTI2LjIzIDE5OCA5MCkiLz4KICAgICAgICA8cmVjdCB3aWR0aD0iMjQ2IiBoZWlnaHQ9IjM4IiB4PSIzNCIgeT0iMTM0IiBmaWxsPSIjRUVGOEZGIiBmaWxsLXJ1bGU9Im5vbnplcm8iIG1hc2s9InVybCgjYikiIHJ4PSIxOSIgdHJhbnNmb3JtPSJyb3RhdGUoLTI2LjIzIDE1NyAxNTMpIi8+CiAgICAgICAgPHJlY3Qgd2lkdGg9IjI0NiIgaGVpZ2h0PSIzOCIgeD0iOTEiIHk9IjE0NyIgZmlsbD0iI0VFRjhGRiIgZmlsbC1ydWxlPSJub256ZXJvIiBtYXNrPSJ1cmwoI2IpIiByeD0iMTkiIHRyYW5zZm9ybT0icm90YXRlKC0zMy41OSAyMTQgMTY2KSIvPgogICAgICAgIDxyZWN0IHdpZHRoPSIyMTIiIGhlaWdodD0iMzgiIHg9IjgzIiB5PSIyMDUiIGZpbGw9IiNFRUY4RkYiIGZpbGwtcnVsZT0ibm9uemVybyIgbWFzaz0idXJsKCNiKSIgcng9IjE5IiB0cmFuc2Zvcm09InJvdGF0ZSgtMzMuNTkgMTg5IDIyNCkiLz4KICAgICAgICA8cmVjdCB3aWR0aD0iMTc2IiBoZWlnaHQ9IjM4IiB4PSI5MSIgeT0iMjUzIiBmaWxsPSIjRUVGOEZGIiBmaWxsLXJ1bGU9Im5vbnplcm8iIG1hc2s9InVybCgjYikiIHJ4PSIxOSIgdHJhbnNmb3JtPSJyb3RhdGUoLTMzLjU5IDE3OSAyNzIpIi8+CiAgICAgICAgPGcgZmlsbC1ydWxlPSJub256ZXJvIiBtYXNrPSJ1cmwoI2IpIj4KICAgICAgICAgICAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNzYgMTI3KSI+CiAgICAgICAgICAgICAgICA8cGF0aCBmaWxsPSIjRjQ3Rjg1IiBkPSJNMTc5LjY5IDBINzAuNTk5QzU5LjQxNS4wNCA0OS42ODkgNy42NjYgNDcgMTguNTA0aDk5Ljg3YzEzLjQxLjA0IDI0LjI3MSAxMC44ODMgMjQuMzEgMjQuMjcydjUwLjQ1N2EyNC4wNiAyNC4wNiAwIDAgMS0uNzA1IDUuNzY3aDkuMjE1YzEzLjQxLS4wMzkgMjQuMjcxLTEwLjg4MyAyNC4zMS0yNC4yNzJWMjQuMjcyQzIwMy45NjEgMTAuODgyIDE5My4xLjAzOSAxNzkuNjkgMHoiLz4KICAgICAgICAgICAgICAgIDxwYXRoIGZpbGw9IiNFNTZDNzciIGQ9Ik0xNzEgNDIuNDI0QzE3MC45NjEgMjguOTUgMTYwLjA5IDE4LjAzOSAxNDYuNjY4IDE4SDQ2LjcwNUEyNC4zMzQgMjQuMzM0IDAgMCAwIDQ2IDIzLjgwM3Y1MC43NzNDNDYuMDM5IDg4LjA1IDU2LjkxIDk4Ljk2MSA3MC4zMzIgOTloOTkuOTYzYy40NjctMS44OTkuNzA0LTMuODQ3LjcwNS01LjgwM1Y0Mi40MjR6Ii8+CiAgICAgICAgICAgICAgICA8cGF0aCBmaWxsPSIjN0VENEZCIiBkPSJNMTMyLjggMTI2LjE2M0g5My40MDFsLTMzLjU2MyAzMS40MDVhOS4wODcgOS4wODcgMCAwIDEtOC4yMDMgMi4yMDUgOS4xMjggOS4xMjggMCAwIDEtNi40NTMtNS41NDRsLTEwLjg3LTI4LjAzOEgyNC4yMDFDMTAuODUgMTI2LjE1Mi4wMzkgMTE1LjI5IDAgMTAxLjg3OVY1MS4zMTJDLjAzOSAzNy45IDEwLjg1MSAyNy4wMzkgMjQuMiAyN2gxMDguNmMxMy4zNDkuMDM5IDI0LjE2MSAxMC45IDI0LjIgMjQuMzEydjUwLjUzOWMtLjAzOSAxMy40MS0xMC44NTEgMjQuMjczLTI0LjIgMjQuMzEyeiIvPgogICAgICAgICAgICAgICAgPHJlY3Qgd2lkdGg9IjEwOCIgaGVpZ2h0PSIxMyIgeD0iMjUiIHk9IjUyIiBmaWxsPSIjRkZGIiByeD0iNi41Ii8+CiAgICAgICAgICAgICAgICA8cmVjdCB3aWR0aD0iMTA4IiBoZWlnaHQ9IjEzIiB4PSIyNSIgeT0iNzAiIGZpbGw9IiNGRkYiIHJ4PSI2LjUiLz4KICAgICAgICAgICAgICAgIDxyZWN0IHdpZHRoPSI2OCIgaGVpZ2h0PSIxMyIgeD0iMjUiIHk9Ijg5IiBmaWxsPSIjRkZGIiByeD0iNi41Ii8+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPgo=" alt="Sản phẩm hiện chưa có hỏi đáp" />
                        <div className="text">Sản phẩm hiện chưa có hỏi đáp.</div>
                        <div className="title">Đặt câu hỏi ngay cho Shop để có thêm thông tin.</div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>({
    product : state.product
})

export default connect(mapStateToProps, null)(ProductTab);