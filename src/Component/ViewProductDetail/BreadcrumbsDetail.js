import React, {Component} from "react"
import "./BreadcrumbsDetail.css"

class BreadcrumbsDetail extends Component{
    render() {
        return (
            <div className="col-12 breadcrumbs-detail">
                <div className="bread-inner">
                    <ul className="bread-list">
                        <li>Trang Chủ<i className="fas fa-arrow-right" /></li>
                        <li className="active">Chi Tiết Sản Phẩm</li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default BreadcrumbsDetail