import React, { Component } from 'react';
import {connect} from "react-redux"
import "./SizeColorProductDetail.css"
import {chooseColor} from "./../../redux/actions"

class ColorProductDetail extends Component {
    render() {
        let attribute = this.props.product.attribute && this.props.product.attribute.length >0 && this.props.product.attribute[1]
        let missing = null
        if(this.props.color === "" && this.props.warning === true){
            missing = <img style={{marginLeft : '2%'}} src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCI+CiAgICA8ZGVmcz4KICAgICAgICA8cGF0aCBpZD0iYSIgZD0iTTEyIDJDNi40OCAyIDIgNi40OCAyIDEyczQuNDggMTAgMTAgMTAgMTAtNC40OCAxMC0xMFMxNy41MiAyIDEyIDJ6bTEgMTVoLTJ2LTJoMnYyem0wLTRoLTJWN2gydjZ6Ii8+CiAgICA8L2RlZnM+CiAgICA8dXNlIGZpbGw9IiNEMDAyMUIiIGZpbGwtcnVsZT0ibm9uemVybyIgeGxpbms6aHJlZj0iI2EiLz4KPC9zdmc+Cg==" />
        }
        return (
            <div className="size-product-detail">
                {attribute && <span className="size-product-detail__name col-lg-2">{attribute.name}:</span>}
                <div className="size-product-detail__size col-lg-10">
                    {attribute && attribute.value.length >0 && attribute.value.map((color,index)=>
                        <div className="button-size" key={index}>
                            <button onClick={()=>this.props.chooseColor(color.name)} className={this.props.color === color.name && "active-color"} type="button">{color.name}</button>
                        </div>                    
                    )}  
                    {missing} 
                </div>  
            </div>
        );
    }
}

const mapStateToProps = state =>({
    product : state.product,
    color : state.color,
    size: state.size,
    warning : state.warning
})

const mapDispatchToProps = dispatch =>({
    chooseColor: (colorProduct)=>dispatch(chooseColor(colorProduct))
})

export default connect(mapStateToProps, mapDispatchToProps)(ColorProductDetail);