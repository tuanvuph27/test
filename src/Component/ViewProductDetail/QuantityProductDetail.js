import React, { Component } from 'react';
import "./QuantityProductDetail.css"
import {connect} from "react-redux"
import {decreaseCount, increaseCount} from "./../../redux/actions"

class QuantityProductDetail extends Component {
    render() {
        let disabled = this.props.count === 1? "disabled" : null
        return (
            <div className="quantity-product-detail">
                <span className="quantity-product-detail__title col-lg-2">Số lượng:</span>
                <div className="quantity-product-detail__option col-lg-10">
                    <button onClick={this.props.decreaseCount} disabled={disabled} className={`minus ${this.props.count===1 && "minus-opacity"}`}>-</button>
                    <input  className="number" value={this.props.count}/>
                    <button onClick={this.props.increaseCount} className="plus">+</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>({
    count : state.count
})

const mapDispatchToProps = dispatch =>({
    increaseCount:()=>dispatch(increaseCount()),
    decreaseCount:()=>dispatch(decreaseCount())
})

export default connect(mapStateToProps, mapDispatchToProps)(QuantityProductDetail);