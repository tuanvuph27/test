import React, {Component} from "react"
import "./HeaderInner.css"
import {Link} from "react-router-dom"

class HeaderInner extends Component{
    render() {
        return (
            <div className="header-inner">
                <div className="cat-nav-head">
                    <div className="menu-area">
                        <nav className="navbar navbar-expand-lg header-inner__nav">
                            <div className="navbar-collapse">	
                                <div className="nav-inner">	
                                    <ul className="nav main-menu menu navbar-nav">
                                        <li className="active"><Link to="/">Trang Chủ</Link></li>
                                        <li><a>Sản Phẩm</a></li>												
                                        <li><a>Dịch Vụ</a></li>
                                        <li><a>Cửa Hàng   <i className="fas fa-angle-down" /><span className="new">New</span></a>
                                            <ul className="dropdown">
                                                <li><Link to="/list">Danh Sách Sản Phẩm</Link></li>
                                                <li><Link to="/cart">Giỏ Hàng</Link></li>
                                                <li><Link to="/">Thoát</Link></li>
                                            </ul>
                                        </li>
                                        <li><a>Thông Tin</a></li>									
                                        <li><a>Liên Hệ</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
}

export default HeaderInner