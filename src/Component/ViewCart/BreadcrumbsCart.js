import React, {Component} from "react"
import "./BreadcrumbsCart.css"

class BreadcrumbsCart extends Component{
    render() {
        return (
            <div className="col-12 breadcrumbs">
                <div className="bread-inner">
                    <ul className="bread-list">
                        <li>Pshop<span style={{color:'rgb(206, 27, 39)'}}>.</span><i className="fas fa-arrow-right" /></li>
                        <li className="active">Giỏ hàng</li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default BreadcrumbsCart