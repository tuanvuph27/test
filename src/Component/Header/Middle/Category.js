import React, {Component} from "react"
import "./Category.css"
import {connect} from "react-redux"
import { getData} from "./../../../redux/actions"
import {Link} from "react-router-dom"

class Category extends Component{
    constructor(props){
        super(props)
        this.state={
            data : ""
        }
    }

    handleAlert = ()=>{
        alert("Bạn chưa nhập từ khóa tìm kiếm")
    }

    render(){
        let {query} = this.props
        let fromUserData = null
        if(query === ""){
            fromUserData = <button className="btnn" onClick={this.handleAlert}><i className="fas fa-search" /></button>           
        } else{
            fromUserData = <Link to="/list" ><button className="btnn" onClick={()=>this.props.getData(1, this.state.data)}><i className="fas fa-search" /></button></Link>
        }
        console.log(this.state.data)
        return(
            <div className="col-lg-8 col-md-7 col-12">
                <div className="category">
                    <div className="category__search">                       
                        <div className="dropdown">
                            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Tất cả
                            </button>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <Link to="/list" style={{textDecoration: "none"}} onClick={()=>this.props.getData(1, "thoi trang nam")} className="dropdown-item" type="button">Nam</Link>
                                <Link to="/list" style={{textDecoration: "none"}} onClick={()=>this.props.getData(1, "thoi trang nu")} className="dropdown-item" type="button">Nữ</Link>
                                <Link to="/list" style={{textDecoration: "none"}} onClick={()=>this.props.getData(1, "do tre em")} className="dropdown-item" type="button">Trẻ em</Link>
                            </div>
                        </div>
                        <div className="search">
                            <input onChange={(e)=>this.setState({data:e.target.value})} placeholder="Tìm Kiếm Sản Phẩm....." type="search" />
                            {fromUserData}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch =>({
    getData : (page, query)=> dispatch(getData(page, query)),
})

export default connect(null, mapDispatchToProps)(Category)