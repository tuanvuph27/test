import React,{Component} from "react"
import MainTopbar from "./Topbar/MainTopbar"
import MainMiddle from "./Middle/MainMiddle"

class MainHeader extends Component{
    render() {
        return (
            <div>
                <MainTopbar />
                <MainMiddle />
            </div>
        );
    }
}

export default MainHeader