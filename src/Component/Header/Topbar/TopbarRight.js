import React, { Component } from "react";
import "./TopbarRight.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {logout} from "./../../../redux/actions"

class TopbarRight extends Component {
  render() {
    return (
      <div className="TopbarRight">
        <ul className="TopbarRight__list-main">
          <li style={{cursor: "pointer"}}>
            <i className="fas fa-map-marker-alt" /> <a>Địa Chỉ</a>
          </li>
          <li style={{cursor: "pointer"}}>
            <i className="far fa-clock" /> <a>Thời Gian</a>
          </li>
          <li>
            {this.props?.user?.username ? (
              <div className="dropdown">
                <a className="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="far fa-user"></i>
                  <span>{this.props?.user?.name}</span>
                </a>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a onClick={this.props.logout} className="dropdown-item">Đăng Xuất</a>
                  <Link to="/profile" className="dropdown-item">Hồ Sơ</Link>
                </div>
              </div>
            ) : (
              <Link to="/login">
                <i className="fas fa-power-off" />{" "}
                Đăng Nhập
              </Link>
            )}
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>({
  logout : ()=>dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(TopbarRight);
