export const GET_DATA = "GET_DATA";
export const FILTER_CHECKBOX = "FILTER_CHECKBOX";
export const DATA_PREVIEW = "DATA_PREVIEW";
export const FILTER_PRICE = "FILTER_PRICE";
export const FILTERLOCATION = "FILTERLOCATION";
export const DATA_PRODUCT_DETAIL = "DATA_PRODUCT_DETAIL";
export const CHANGE_IMG_DETAIL = "CHANGE_IMG_DETAIL";
export const DECREASE_COUNT = "DECREASE_COUNT";
export const INCREASE_COUNT = "INCREASE_COUNT";
export const SIZE = "SIZE";
export const COLOR = "COLOR";
export const ADD_TO_CART = "ADD_TO_CART";
export const WARNING = "WARNING";
export const FIX_WARNING = "FIX_WARNING";
export const CHANGE_ITEM_QUANTITY = "CHANGE_ITEM_QUANTITY";
export const REMOVE_ITEM = "REMOVE_ITEM";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const SEARCH = "SEARCH";
export const SET_ORDER_NAMEAZ = "SET_ORDER_NAMEAZ"
export const SET_ORDER_NAMEZA = "SET_ORDER_NAMEZA"
export const SET_ORDER_PRICEAZ = "SET_ORDER_PRICEAZ"
export const SET_ORDER_PRICEZA = "SET_ORDER_PRICEZA"

export const getData = (page, query) => {
  return (dispatch) => {
    fetch(`http://localhost:4000/products?p=${page}&q=${query}`)
      .then((res) => res.json())
      .then((json) => {
        dispatch({
          type: GET_DATA,
          productList: json.data,
        });
      });
  };
};

export const logout = ()=>({
  type : LOGOUT
})

export const login = (data) => {
  return (dispatch) => {
    let username = data.username;
    let password = data.password;

    fetch(`http://localhost:4000/login`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",        
      },
      body: JSON.stringify({ username: username, password: password }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
      dispatch({
        type: LOGIN,
        user: data,
        });
      });
  };
};

export const dataProductDetail = (id) => {
  return (dispatch) => {
    fetch(`http://localhost:4000/product/${id}`)
      .then((res) => res.json())
      .then((json) => {
        dispatch({
          type: DATA_PRODUCT_DETAIL,
          product: json,
        });
      });
  };
};

export const dataPreview = (page, query) => {
  return (dispatch) => {
    fetch(`http://localhost:4000/products?p=${page}&q=${query}`)
      .then((res) => res.json())
      .then((json) => {
        dispatch({
          type: DATA_PREVIEW,
          productPreview: json.data,
        });
      });
  };
};

export const filterCheckbox = (key, value) => ({
  type: FILTER_CHECKBOX,
  key,
  value,
});

export const search = (dataUser) => ({
  type: SEARCH,
  dataUser,
});

export const filterPrice = (min, max) => ({
  type: FILTER_PRICE,
  min,
  max,
});

export const filterLocation = (local) => ({
  type: FILTERLOCATION,
  local,
});

export const changeImgDetail = (img) => ({
  type: CHANGE_IMG_DETAIL,
  img,
});

export const decreaseCount = () => ({
  type: DECREASE_COUNT,
});

export const increaseCount = () => ({
  type: INCREASE_COUNT,
});

export const chooseSize = (sizeProduct) => ({
  type: SIZE,
  sizeProduct,
});

export const chooseColor = (colorProduct) => ({
  type: COLOR,
  colorProduct,
});

export const addToCart = (payload) => ({
  type: ADD_TO_CART,
  payload,
});

export const missingAttribute = () => ({
  type: WARNING,
});

export const fixWarning = () => ({
  type: FIX_WARNING,
});

export const changeItemQuantity = (
  itemId,
  itemQuantity,
  itemSize,
  itemColor
) => ({
  type: CHANGE_ITEM_QUANTITY,
  itemId,
  itemQuantity,
  itemSize,
  itemColor,
});

export const removeItem = (itemId, itemSize, itemColor) => {
  return {
    type: REMOVE_ITEM,
    itemId,
    itemSize,
    itemColor,
  };
};

export const setOrderNameAZ = ()=>({
  type: SET_ORDER_NAMEAZ ,
})

export const setOrderNameZA = ()=>({
  type: SET_ORDER_NAMEZA ,
})

export const setOrderPriceAZ = ()=>({
  type: SET_ORDER_PRICEAZ ,
})

export const setOrderPriceZA = ()=>({
  type: SET_ORDER_PRICEZA ,
})
