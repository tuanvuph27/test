import reducer from "./reducer"
import {createStore, compose, applyMiddleware} from "redux"
import thunk from "redux-thunk"
const middleware = [thunk]
const enhancer = compose(applyMiddleware(...middleware))
const store = createStore(reducer,enhancer)

export default store